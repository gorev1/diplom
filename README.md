# Дипломная работа Горева Ярослава

## Запуск всего проекта

Выполняем в консоли:

```bash
git clone git@gitlab.com:gorev1/diplom.git
cd diplom
git submodule init
git submodule update
docker-compose up
```

Далее открываем http://localhost:3000, на этой странице расположен фронтовая часть проекта

Ознакомиться со swagger бекенда можно по http://localhost:7000/docs

Для первоначальное авторизации на сайте можно ввести email: `admin` и password: `admin`, тогда появится добавлять
новых пользователей и новые записи
